This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Setting up the environment

Make sure you run `$ npm install` before you begin.

The project is divided in 2 sections:

1. A stand-alone REPL script that handles all user input as requested in the Test definition document.
2. A React application created to answer Part 2 of the test (I know, not required but couldn't help myself).

Both instances use the 'game-logic' module (src/GameLogic/game-logic) which encapsulates the logic of the game
and can be used by both instances as a standard ES6 module.

For optimization purposes, my approach to the FIB detection was to build an array during startup with 1000 Fibonacci numbers and then just look up in that array for the user-typed value. This to avoid re-computing a new
Fibonacci sequence every time the user entered a value.

# Part 1. Running the REPL

After installing all the required dependencies you just need to run:

`$ npm run run-game`

This command should compile the typescript and run the resulting .js file for you.


# Part 2. Running the WebApp

The web app is a standard react application that uses same game-logic.ts module but adds a nice UI on top of it.

To start the local dev server just run: `$ npm start`

### Not implemented:

- Unit tests for the react components
- Not optimized
- A 'Quit' button from the interface


# Answers to Questions In Part 2.

1. From the very beginning, I designed the game logic to be reusable. The library only exposes a few methods to define the initial parameters and communication with other modules is done via event listeners, allowing the programmer to implement the presentation layer in any way they see fit.

2. First of all, unit tests! the game-logic and helper libraries have all unit tests to ensure the code complies with the specifications. 

Having said that, due to time constraints, I didn't get to add unit tests to the react application. I normally use jest and snapshot testing to test components.

I would then set up CodeBuild in AWS (via buildspec.yml) to run tests on every deployment and possibly a combination
of CodePipeline & CodeDeploy for continuous delivery to CloudFront.

3. I enjoyed it. Being honest, I'm not used to using typescript with react so the challenge motivated me more to implement the react version for Part 2. Will probably continue using it.

The test was clear enough, it did take me a few re-reads to understand what you wanted, and the examples helped a lot.

Thanks for the opportunity!