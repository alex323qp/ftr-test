import React, { Fragment } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import './Layout.css';
import '../assets/style/animated.css';
interface Props {
	children?: JSX.Element[];
}

const Layout = (props: Props) => {
	return (
		<Fragment>
			<div className="navbar navbar-default navbar-fixed-top nav-bar" />
			<div className="container centered">
				<div className="row h-100 align-items-center">
					<div className="col-12">
						{ props.children }
					</div>
				</div>
			</div>
		</Fragment>
	)
}

export default Layout