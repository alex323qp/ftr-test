import React, { useRef, useEffect } from 'react'
import Card from '../components/Card/Card';
import UserInput from '../components/UserInput'
import FormatListNumberedRtlIcon from '@material-ui/icons/FormatListNumberedRtl';
import { animateElement } from '../common/utils';
import ScreenContainer from './ScreenContainer';

interface Props {
	children?: JSX.Element[];
	onNext?: Function
}

const InitialSequencePrompt = (props: Props) => {
	const cardRef = useRef<HTMLDivElement>(null);
	const txtFirstSeq = useRef<HTMLInputElement>(null);

	useEffect(()=>{
		if(cardRef.current !== null){
			animateElement(cardRef.current, 'backInRight', '0.5s')
		}
	}, [])

	const customTitle = (
		<div style={{alignContent: 'center'}}>
			<FormatListNumberedRtlIcon /> Initial Number
		</div>
	)

	const goNextHandler = (): void =>{
		cardRef.current && animateElement(cardRef.current, 'backOutLeft', '0.5s').then(()=>{
			props.onNext && props.onNext(txtFirstSeq?.current?.value);
		});
	}

	return (
		<ScreenContainer ref={cardRef}>
			<Card title={customTitle}>
				<p>Please enter the first number</p>
				<div className="form-group pt-4">
					<UserInput defaultValue={15} type="number" ref={txtFirstSeq} name="freq" placeholder="Sequence"/>
				</div>
				<div className="form-group pt-4">
					<button onClick={goNextHandler} className="btn btn-block btn-primary btn-xl">
						Apply
					</button>
				</div>

			</Card>
		</ScreenContainer>
	)
}

export default InitialSequencePrompt
