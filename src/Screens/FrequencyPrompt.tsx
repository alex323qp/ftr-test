import React, { useRef, useEffect } from 'react'
import Card from '../components/Card/Card';
import UserInput from '../components/UserInput'
import AccessAlarmIcon from '@material-ui/icons/AccessAlarm';
import { animateElement } from '../common/utils';
import ScreenContainer from './ScreenContainer';

interface Props {
	children?: JSX.Element[];
	onNext?: Function
}

const FrequencyPrompt = (props: Props) => {
	const cardRef = useRef<HTMLDivElement>(null);
	const txtFreq = useRef<HTMLInputElement>(null);

	useEffect(()=>{
		if(cardRef.current !== null){
			animateElement(cardRef.current, 'backInRight')
		}
	}, [])

	const customTitle = (
		<div style={{alignContent: 'center'}}>
			<AccessAlarmIcon /> Output Frequency
		</div>
	)

	const goNextHandler = (): void =>{
		cardRef.current && animateElement(cardRef.current, 'backOutLeft', '0.5s').then(()=>{
			props.onNext && props.onNext(txtFreq?.current?.value);
		});
	}

	return (
		<ScreenContainer ref={cardRef}>
			<Card title={customTitle}>
				<p>Please input the number of time in seconds between emitting numbers and their frequency.</p>
				<div className="form-group pt-4">
					<UserInput defaultValue={2} ref={txtFreq} name="freq" placeholder="Frequency" type="number"/>
				</div>
				<div className="form-group pt-4">
					<button onClick={goNextHandler} className="btn btn-block btn-primary btn-xl">
						Apply
					</button>
				</div>
			</Card>
		</ScreenContainer>
	)
}

export default FrequencyPrompt
