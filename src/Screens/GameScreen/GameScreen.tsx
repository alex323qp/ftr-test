import React, { useRef, Fragment, useState, useEffect } from 'react'
import Card from '../../components/Card/Card';
import classes from './GameScreen.module.css';
import SendIcon from '@material-ui/icons/Send';
import gameLogic from '../../GameLogic/game-logic';

interface Props {
	initialFreq: number;
	initialSeq: number;
	onRestart: Function;
}

const GameScreen = (props: Props) => {
	const cardRef = useRef<HTMLDivElement>(null);
	const userInputRef = useRef<HTMLInputElement>(null);
	const [isPaused, setIsPaused] = useState(false);
	const [display, setDisplay] = useState('');
	
	const appendToDisplay = (msg: string) => {
		setDisplay((text: string) => `${text}\n>> ${msg}`)
	}


	useEffect(()=>{
		gameLogic.setSequence(props.initialSeq);
		gameLogic.setFrequency(props.initialFreq);
		gameLogic.onPrint(appendToDisplay);
		gameLogic.onFibDetected(()=> appendToDisplay('FIB'));
		gameLogic.startDaemon();
		
		return () =>{
			gameLogic.terminate();
		}
	}, [props])

	const pauseHandler = ()=>{
		if(isPaused){
			appendToDisplay('timer resumed');
			gameLogic.startDaemon();
		}else{
			appendToDisplay('timer halted');
			gameLogic.stopDaemon();
		}
		
		setIsPaused( !isPaused);
	}
	
	const sendCommandHandler = ()=>{
		if(userInputRef.current){
			const text = userInputRef.current.value;
			if(Number.isNaN(Number(text))) return appendToDisplay('*** Invalid Type ***');
			gameLogic.setSequence(parseInt(text || ''));
			userInputRef.current.value = '';
		}
	}

	return (
		<Fragment>
			<div ref={cardRef} className="row justify-content-center">
				<div className="col-xs-auto">
					<Card title="Enjoy the game!">
						<p>Waiting for command...</p>
						<div className="form-group pt-4">
							<textarea readOnly className={classes.Display} rows={4} defaultValue={display}></textarea>
						</div>
						<div className="form-group pt-4">
							<button onClick={pauseHandler} className="btn btn-block btn-primary btn-xl">
								{ isPaused ? 'Resume' : 'Pause' }
							</button>
						</div>
					</Card>
				</div>
			</div>

			<div className="row justify-content-center">
				<div className="col-lg-6 col-xs-12">
					<div className={classes.StdInContainer}>
						<input ref={userInputRef} type="text" className={classes.StdIn} placeholder="Please enter the next number..."/>
						<button onClick={sendCommandHandler} className={`btn btn-light ${classes.SendButton}`}>
							<SendIcon />
						</button>
					</div>
				</div>
			</div>
		</Fragment>
	)
}

export default GameScreen
