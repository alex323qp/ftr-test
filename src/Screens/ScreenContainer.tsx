import React, { forwardRef } from 'react'

interface Props {
	children?: JSX.Element | JSX.Element[];
}

const ScreenContainer = forwardRef((props: Props, ref: React.Ref<HTMLDivElement>) => {
	return (
		<div ref={ref} className="row justify-content-center">
			<div className="col-xs-auto">
				{ props.children }
			</div>	
		</div>
	)
})

export default ScreenContainer
