import generateSequence, { numberInSequence } from './fibonacci';

it('Checks if fibonacci tables are generated correctly', ()=> {
	const sequence = generateSequence(10);
	const list = [1, 1,	2,	3,	5,	8,	13,	21,	34,	55];
	expect(sequence).toEqual(list);
});


it('Checks if a number can be found in a generated table', ()=>{
	const sequence = generateSequence(10);
	expect(numberInSequence(5, sequence)).toBe(true);
	expect(numberInSequence(8, sequence)).toBe(true);
	expect(numberInSequence(55, sequence)).toBe(true);
	expect(numberInSequence(0, sequence)).toBe(false);
	expect(numberInSequence(7, sequence)).toBe(false);
	expect(numberInSequence(6, sequence)).toBe(false);
	expect(numberInSequence(60, sequence)).toBe(false);
})