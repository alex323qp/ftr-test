/*
 * Written by Alexander Agudelo < mario323qp@gmail.com >, 2020
 * Date: 28/May/2020
 * Last Modified: 28/05/2020, 10:06:21 am
 * Modified By: Alexander Agudelo
 * Description:  FTR Coding Test
 */

import generateSequence, { numberInSequence } from './fibonacci';

type Callback = (msg: string) => void;

let frequency: number = 0;
let fibTable: number[] = generateSequence(1000);
let timer: NodeJS.Timeout;
let printCb: Callback | undefined;
let fibDetectCb: Callback | undefined;

interface Sequence {
	[key:string]: number
}

let sequences:Sequence = {};

/**
 * Initiates the daemon
 */
const startDaemon = (): void =>{
	if(!frequency) throw new Error('Please define a frequency first');
	if(!Object.keys(sequences).length) throw new Error('Please enter a sequence first');
	stopDaemon();
	timer = setInterval(printSequence, frequency);
}

/**
 * Calculates 'in frequency descending order, the list of numbers and their frequency'
 * @returns the sequence information
 */
const calculateSequence = (): string =>{
	let orderedSeq: string[] = Object.keys(sequences).sort((a, b) => sequences[b] - sequences[a]);
	const results: string[] = [];
	for(const s of orderedSeq){
		results.push(`${s}:${sequences[s]}`);
	}

	return results.join(", ");
}

/**
 * Called every @frequency seconds.
 */
const printSequence = (): void => {
	const msg = calculateSequence();
	printCb && printCb(msg);
}

/**
 * Aborts the active timer
 */
const stopDaemon = (): void =>{
	clearTimeout(timer);
}

/**
 * Stops the timer and sets a new frequency interval
 * @param fq the interval time in seconds
 */
const setFrequency = ( fq: number): void =>{
	stopDaemon();
	frequency = fq * 1000;
}

/**
 * Records sequences
 * @param s a new sequence to record
 */
const setSequence = (s: number): void =>{
	sequences[s] = (sequences[s] || 0) + 1;
	if(fibDetectCb && numberInSequence(s, fibTable)) fibDetectCb(String(s));
}

/**
 * Sets an event handler that will be called when the system is ready
 * to print the next sequence.
 * @param cb function to call before printing something
 */
const onPrint = (cb: Callback): void => {
	printCb = cb;
}

/**
 * Sets an event listener for fibonacci numbers detected in 
 * sequences.
 * @param cb function to call when a fibonacci number is detected
 */
const onFibDetected = (cb: Callback): void =>{
	fibDetectCb = cb;
}

/**
 * Aborts the timers, resets the frequency,
 * clears event listeners and prints a summary for the last time
 * @returns a summary of sequences
 */
const terminate = (): string =>{
	stopDaemon();
	const msg = calculateSequence();
	frequency = 0;
	printCb = undefined;
	fibDetectCb = undefined;
	sequences = {};
	return msg;
}

export default {
	setFrequency,
	setSequence,
	startDaemon,
	stopDaemon,
	terminate,
	onPrint,
	onFibDetected,
}