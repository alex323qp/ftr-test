/**
 * Generates a list of fibonacci sequences
 * @param n the number of items in the sequence
 */
const generate = (n: number):number[] =>{
	const s = [1, 1];
	if(n < 2) return s;

	for(let i = 2; i < n; i++){
		s.push(s[i-2] + s[i-1])
	}

	return s;
}

/**
 * Checks if @n exists in @t.
 * @param n the number to look for
 * @param t a list of fibonacci numbers
 */
const numberInSequence = (n:number, t:number[]): boolean =>{
	return !!t.find(i => i === n);
}

export default generate;
export {
	numberInSequence
}