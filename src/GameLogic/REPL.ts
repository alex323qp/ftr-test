import fibApp from './game-logic';

let onUserInputCb:Function;

// Read user input
process.stdin.setEncoding ("utf-8");
process.stdin.on ("data", d => {
	onUserInputCb && onUserInputCb(d);
});

// Prints the given texts and waits for user input
const readLine = (question: string): Promise<string> => {
	return new Promise((resolve) =>{
		// Store the original callback
		const prevCb = onUserInputCb;
		console.log(question);
		onUserInputCb = (line: string): void =>{
			// Restore callback
			onUserInputCb = prevCb;
			resolve(line);
		}
	});
}

// Waits until the word 'resume' is typed
const waitForResume = async (msg: string): Promise<void> =>{
	const line = await readLine(msg);
	if(line === 'resume\n') {
		console.log('>> timer resumed');
		return;
	}

	console.log(`'${line.trim()}' not sure what that is. Did you mean 'resume'?`);
	return waitForResume('');
}

// Checks if the given input string is a numeric value
const isNumeric = (value: string): boolean => !(Number.isNaN(Number(value)) || value === '\n');

/**
 * Waits until a valid numeric value is typed
 * @param msg a message to print before waiting for the user input
 */
const getNumericValue = async (msg: string): Promise<number> =>{
	const value: string = await readLine(`${msg}`);
	if( !isNumeric(value)) {
		console.log('** Please enter a numeric value **');
		return getNumericValue('');
	}

	if(value === '0\n'){
		console.log("*** Please enter a non-zero value ***");
		return getNumericValue('');
	}

	return parseInt(value);
}


// Init
const main = async ()=> {
	const freq: number = await getNumericValue('>> Please input the number of time in seconds between emitting numbers and their frequency');
	const firstSequence: number = await getNumericValue('>> Please enter the first number');
	fibApp.setFrequency(freq);
	fibApp.setSequence(firstSequence);
	fibApp.startDaemon();

	// A fibonacci number has been detected
	fibApp.onFibDetected((): void =>{
		console.log('>> FIB');
	})

	// Waits for std input
	onUserInputCb = async (line: string) => {
		if(line === 'halt\n'){
			fibApp.stopDaemon()
			await waitForResume('>> timer halted');
			return fibApp.startDaemon();
		}

		if(line === 'quit\n'){
			const summary = fibApp.terminate();
			console.log(`>> ${summary}`);
			console.log('>> Thanks for playing, press any key to exit.');
			await readLine('');
			process.exit(0);
		}

		if( !isNumeric(line) )  {
			return console.log("*** Invalid, please enter a numeric type ***");
		}

		fibApp.setSequence( parseInt(line) );
		console.log('>> Please enter the next number');
	}

	// Prints generated sequences
	fibApp.onPrint( async (msg: string) => {
		console.log(`>> ${msg}`);
	})
}

// call func only if executed directly (node REPL.js)
if (require.main === module) {
  main();
}

// For unit tests
export default main;