import fibApp from './game-logic.ts';

jest.useFakeTimers();

describe('Tests the game logic', () => {
	beforeEach(() => {
		jest.clearAllTimers();
		fibApp.terminate();
	})

	// ClearInterval doesn't work inside the tests
	// so we mock it!
	const mockedHalt = ()=>{
		fibApp.stopDaemon(); // This doesn't affect (clear) the jest interval
		jest.clearAllTimers(); // This does!
	}

	it('throws an error if no interval or initial sequence have been set', () => {
		expect(fibApp.startDaemon).toThrow('Please define a frequency first');
		fibApp.setFrequency(2);
		expect(fibApp.startDaemon).toThrow('Please enter a sequence first');
		fibApp.setSequence(1);
		expect(fibApp.startDaemon).not.toThrow();
	})

	it('the callback function is called every second', () => {
		const messageCb = jest.fn();
		fibApp.onPrint(messageCb);
		fibApp.setFrequency(1);
		fibApp.setSequence(15);
		fibApp.startDaemon();
		
		// Should not be called yet
		expect(messageCb).not.toBeCalled();
		
		// fast-forward 
		jest.advanceTimersByTime(1000);
		expect(messageCb).toHaveBeenCalledTimes(1);
		jest.advanceTimersByTime(2000);
		expect(messageCb).toHaveBeenCalledTimes(3);
	})

	it('can halt the count', () => {
		const messageCb = jest.fn();
		fibApp.onPrint(messageCb);
		fibApp.setFrequency(1);
		fibApp.setSequence(15);
		fibApp.startDaemon();

		jest.advanceTimersByTime(1000);
		expect(messageCb).toHaveBeenCalledTimes(1);
		
		mockedHalt();

		jest.advanceTimersByTime(5000);
		expect(messageCb).toHaveBeenCalledTimes(1);
	})

	it('can resume the count', () => {
		const messageCb = jest.fn();
		fibApp.onPrint(messageCb);
		fibApp.setFrequency(1);
		fibApp.setSequence(15);
		fibApp.startDaemon();

		jest.advanceTimersByTime(1000);
		expect(messageCb).toHaveBeenCalledTimes(1);
		
		// Need to mock the 'clearInterval' inside
		mockedHalt();

		jest.advanceTimersByTime(5000);
		expect(messageCb).toHaveBeenCalledTimes(1);

		fibApp.startDaemon();
		jest.advanceTimersByTime(5000);
		expect(messageCb).toHaveBeenCalledTimes(6);
	})

	it('fibonacci numbers are detected', () => {
		const fibDetected = jest.fn();
		fibApp.onFibDetected(fibDetected);
		// 1, 1,	2,	3,	5,	8,	13,	21,	34,	55
		fibApp.setSequence(4)
		expect(fibDetected).not.toHaveBeenCalled()
		fibApp.setSequence(5);
		fibApp.setSequence(13);
		fibApp.setSequence(21);
		fibApp.setSequence(57);		// Invalid
		expect(fibDetected).toHaveBeenCalledTimes(3);
	})

	it('the output sequence is in descending order', () => {
			const messageCb = jest.fn();
			fibApp.onPrint(messageCb);
			fibApp.setFrequency(1);

			fibApp.setSequence(10);
			fibApp.setSequence(10);
			fibApp.setSequence(8);

			fibApp.startDaemon();
			// Time travel to 1 sec into the future
			jest.advanceTimersByTime(1000);

			const expectedStr = '10:2, 8:1';
			expect(messageCb).toHaveBeenCalledWith(expectedStr);
	})

	it('shows a correct sequence summary every 1 second', () => {

	})

	it('shows a summary message after terminating the game', ()=>{
			fibApp.setFrequency(1);

			fibApp.setSequence(10);
			fibApp.setSequence(10);
			fibApp.setSequence(8);
			fibApp.setSequence(8);
			fibApp.setSequence(8);

			const result = fibApp.terminate();
			expect(result).toBe('8:3, 10:2')
	})
})