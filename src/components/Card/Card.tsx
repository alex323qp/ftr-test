import React from 'react'
import classes from './Card.module.css';

interface Props {
	content?: string;
	children?: JSX.Element[];
	title?: JSX.Element | string;
}

const Card = (props: Props) => {
	return (
		<div className={`${classes.Card} ${classes.BorderColor}`}>
			<div className={classes.Header}>
				<div className={`${classes.HeadTitle} ${classes.CenteredHeaderContent}`}>
					{ props.title }
				</div>
			</div>

			<div className={classes.Body}>
				{ props.children }
			</div>
		</div>
	)
}

export default Card
