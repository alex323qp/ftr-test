import React, { forwardRef } from 'react'

interface Props {
	name: string;
	placeholder: string;
	style?: object;
	type?: string;
	defaultValue?: string | number;
}

const UserInput = forwardRef((props: Props, ref: React.Ref<HTMLInputElement>) => {
	return (
		<input defaultValue={props.defaultValue} ref={ref} className="form-control fancyButton" type={props.type || 'text'} name={props.name} placeholder={props.placeholder} style={{...props.style, textAlign: 'center'}} ></input>
	)
})

export default UserInput
