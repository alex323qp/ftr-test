

/**
 * Animates a html element
 * @param element the element to animate
 * @param animation name of the animation (eg: 'backInRight')
 * @param duration duration in format: '1s', '0.5s', etc
 * @param prefix changes the animation prefix
 */
const animateElement = (element: HTMLElement, animation: string, duration = '', prefix = 'animate__'): Promise<void> => {
  return new Promise((resolve, reject) => {
    const animationName = `${prefix}${animation}`;


    element.classList.add(`${prefix}animated`, animationName);
		if(duration !== '') element.style.setProperty('--animate-duration', duration);

    // Remove classes once the animation ends
    function handleAnimationEnd() {
      element.classList.remove(`${prefix}animated`, animationName);
      element.removeEventListener('animationend', handleAnimationEnd);

      resolve();
    }

    element.addEventListener('animationend', handleAnimationEnd);
	});
}

export { 
	animateElement
}