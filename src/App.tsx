import React, { useState } from 'react';
import Layout from './Layout/Layout';
import FrequencyPrompt from './Screens/FrequencyPrompt';
import InitialSequencePrompt from './Screens/InitialSequencePrompt';
import GameScreen from './Screens/GameScreen/GameScreen';

function App() {
  const [step, setStep ] = useState(0);
  const [frequency, setFrequency] = useState<number>(0);
  const [initialSeq, setInitialSeq] = useState<number>(0);

  const goNext = ()=>{
    setStep( s => s + 1);
  }

  const setFrequencyHandler = ( freq: number )=>{
    setFrequency(freq);
    goNext()
  }

  const setInitialSeqHandler = ( seq: number )=>{
    setInitialSeq(seq);
    goNext()
  }

  const restartGameHandler = ()=>{
    setInitialSeq(0);
    setStep(0);
  }

  return (
    <Layout>
      { step === 0 ? <FrequencyPrompt onNext={setFrequencyHandler}/> : null }
      { step === 1 ? <InitialSequencePrompt onNext={setInitialSeqHandler}/> : null }
      { step === 2 ? <GameScreen initialSeq={initialSeq} initialFreq={frequency} onRestart={restartGameHandler}/> : null }
      {/* */}
    </Layout>
  );
}

export default App;
